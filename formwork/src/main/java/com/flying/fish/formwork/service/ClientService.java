package com.flying.fish.formwork.service;

import com.flying.fish.formwork.base.BaseService;
import com.flying.fish.formwork.dao.ClientDao;
import com.flying.fish.formwork.entity.Client;
import org.springframework.stereotype.Service;

/**
 * @Description 客户端管理业务类
 * @Author jianglong
 * @Date 2020/05/16
 * @Version V1.0
 */
@Service
public class ClientService extends BaseService<Client,String,ClientDao> {
}
