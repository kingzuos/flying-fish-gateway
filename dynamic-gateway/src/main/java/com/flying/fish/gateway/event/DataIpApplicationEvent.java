package com.flying.fish.gateway.event;

import org.springframework.context.ApplicationEvent;

/**
 * @Description 创建自定义IP事件
 * @Author jianglong
 * @Date 2020/05/28
 * @Version V1.0
 */
public class DataIpApplicationEvent extends ApplicationEvent {
    /**
     * Create a new ApplicationEvent.
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public DataIpApplicationEvent(Object source) {
        super(source);
    }
}
